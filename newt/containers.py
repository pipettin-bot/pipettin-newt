# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from .schemas.containers import validate_container

def base_container(
    name: str,
    container_type: str,
    length: float,
    maxVolume: float,
    activeHeight: float,
    description: str = "Parameters describing a kind of liquid container",
    ):
    data = {
        "name": name,
        "description": description,
        "type": container_type,
        "length": length,
        "maxVolume": maxVolume,
        "activeHeight": activeHeight
    }

    validate_container(data)

    return deepcopy(data)


#### Specific contents ####

# TODO: base this on containers.
def container_tube(
    name: str,
    length: float,
    maxVolume: float,
    activeHeight: float,
    description: str = "Parameters describing a kind of tube",
    **kwargs
    ):

    data = base_container(
        name=name,
        container_type="tube",
        description=description,
        length=length,
        maxVolume=maxVolume,
        activeHeight=activeHeight,
    )

    data.update(**kwargs)

    validate_container(data)

    return deepcopy(data)
