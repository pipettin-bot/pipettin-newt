# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

import time
import math
from copy import deepcopy

def guess_shape(platform_data: dict):
    if platform_data.get("diameter", 0):
        size_x = size_y = platform_data["diameter"]
        shape = "circular"
    else:
        size_x, size_y = platform_data["width"], platform_data["length"]
        shape = "rectangular"
    return size_x, size_y, shape

def adjust_pos_by_first_item(platform_item, platform_definition,
                             content_x=None, content_y=None,
                             content_position=None,
                             tool_offset=None):
    """
    Adjust the position of a platform item by the measured position of the first item.
    Calculate and replace the origin of a grid-type platform from the position of a content.
    """

    if content_position is None:
        content_position = {"col": 1, "row": 1}
    if tool_offset is None:
        tool_offset = {"x": 0, "y": 0}

    print("Original position:", platform_item["position"])

    # Look for a content with matching position
    contents = platform_item["content"]
    content = None
    for a_content in contents:
        if content_position == a_content["position"]:
            content = a_content
            break
    if content is None:
        print("No content matched, returning none.")
        return None

    if content_x is not None:
        x = content_x
        # Calculate X of platform origin compatible with measurement
        x -= (content["position"]["col"]-1) * platform_definition['wellSeparationX']
        x -= platform_definition['firstWellCenterX']
        # Apply the tool's offsets
        x -= tool_offset["x"]
        # Apply the adjustment
        platform_item["position"]["x"] = x

    if content_y is not None:
        # Calculate Y of platform origin compatible with measurement
        y = content_y
        y -= (content["position"]["row"] - 1) * platform_definition['wellSeparationY']
        y -= platform_definition['firstWellCenterY']

        # Apply the tool's offsets
        y -= tool_offset["y"]

        # Apply the adjustment
        platform_item["position"]["y"] = y

    print("Updated position:", platform_item["position"])

    return platform_item["position"]

def get_content(data, value=1, key="index"):
    """Returns the dictionary of a list with a matching key/value pair."""
    result = None
    for datum in data:
        if datum[key] == value:
            result = datum
    return result

def get_content_by_pos(platform_item, col, row):
    return get_content(platform_item["content"], value={'col': col, 'row': row}, key="position")

def add_content(content, platform_item, platform_definition,
                amount=1, name=None):

    # Default name for the inserted contents
    if name is None:
        name = content["name"]

    # Work on a full copy
    platform_item = deepcopy(platform_item)

    # Fill with content
    i=0
    n=amount
    for row in range(platform_definition["wellsRows"]):
        for col in range(platform_definition["wellsColumns"]):
            i+=1

            # Check if enough contents have been added
            if n < 1:
                break

            # Make name for the content
            if amount == 1:
                a_name = name
            else:
                a_name = name + str(i)


            # Skip existing indexes/col/pos/name
            if get_content(platform_item["content"], value={'col': col+1, 'row': row+1}, key="position"):
                continue
            if get_content(platform_item["content"], value=i, key="index"):
                continue
            if get_content(platform_item["content"], value=a_name, key="name"):
                continue

            # Add content to free slot
            a_content = deepcopy(content)
            a_content["position"] = {"col": col+1, "row": row+1}
            a_content["name"] = a_name
            a_content["index"] = i
            platform_item["content"].append(a_content)
            n -= 1

    if n > 0:
        print(f"Failed to add {n} contents. Not enough free/unique slots in platform item.")

    return platform_item


def fill_with_content(content, platform_item, platform_definition):

    # Look for a content with matching position, and stop if found.
    if len(platform_item["content"]) > 0:
        print("Preexisting content matched, returning none. Platform item contents must be empty.")
        return None

    # Work on a full copy
    platform_item = deepcopy(platform_item)

    amount = platform_definition["wellsRows"] * platform_definition["wellsColumns"]
    platform_item = add_content(content, platform_item, platform_definition,
                                name=content["type"], amount=amount)

    # Fill with content
    # i=0
    # for row in range(platform_definition["wellsRows"]):
    #     for col in range(platform_definition["wellsColumns"]):
    #         i+=1
    #         a_content = deepcopy(content)
    #         a_content["position"] = {"col": col+1, "row": row+1}
    #         a_content["name"] = content["type"] + str(i)
    #         a_content["index"] = i
    #         platform_item["content"].append(a_content)

    return platform_item


def update_optional(child: dict, parent: dict, optional: list = None):
    """Update the 'optional' field in a JSON schema object."""
    child = deepcopy(child)
    parent = deepcopy(parent)
    if not optional:
        optional = []
    else:
        optional = deepcopy(optional)

    parent_props = list(parent["properties"])
    parent_required = parent["required"]
    parent_optional = [p for p in parent_props if p not in parent_required]

    new_optional = list(set(parent_optional + optional))

    child_props = list(child["properties"])
    new_required = [p for p in child_props if p not in new_optional]

    child["required"] = new_required

    # print("parent_optional", parent_optional)
    # print("parent_required", parent_required)
    # print("new_optional", new_optional)
    # print("new_required", new_required, "\n")

    return child

def make_date():
    date = time.asctime() + " UTC" + time.strftime("%z", time.localtime())
    return date

def draw_ascii_workspace(
    workspace: dict, platforms: list,
    downscale_factor: float = 20,
    width_scaler: float = 2.5,
    anchor_char: str = "@",
    item_char: str = "¬",
    empty_char: str = " ",
    indent:int = 0):
    """
    Generates an ASCII art representation of platform items in a workspace.

    Args:
        downscale_factor (float): Factor used to scale the units down.

    Returns:
        str: ASCII art representation of the workspace and platform items.

    Example:
        MK3 Baseplate: 488mm x 290mm x 8mm (XYZ)
        .----------------------------------------------------.
        |                                                    |
        |                                                    |
        |                                                    |
        |                                        ¬¬¬¬¬¬¬¬¬¬¬ |
        |                                        ¬¬¬¬¬¬¬¬¬¬¬ |
        |                      @@@@¬¬¬¬¬¬¬¬¬     ¬¬¬¬¬¬¬¬¬¬¬ |
        |                      @¬¬¬¬¬¬¬¬¬¬¬¬     ¬¬¬¬¬¬¬¬¬¬¬ |
        |                      ¬¬¬¬¬¬¬¬¬¬¬¬¬     ¬¬¬¬¬¬¬¬¬¬¬ |
        |                      ¬¬¬¬¬¬¬¬¬¬¬¬¬                 |
        |                                                    |
        |      @@@@¬¬¬¬¬¬¬¬¬   @@@@¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬       |
        |      @¬¬¬¬¬¬¬¬¬¬¬¬   @¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬       |
        |      ¬¬¬¬¬¬¬¬¬¬¬¬¬   ¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬       |
        '----------------------------------------------------'
        Origin: (000.000, 000.000, 000.000)
    """

    platform_items = sorted(workspace["items"], key=lambda item: item.get("type") != "ANCHOR")

    # Convert workspace dimensions to ASCII grid size (1 char = 20 mm)
    width = math.ceil(width_scaler * workspace["width"] / downscale_factor)
    length = math.ceil(workspace["length"] / downscale_factor)

    # Create an empty grid to represent the workspace
    grid = [[empty_char for _ in range(width)] for _ in range(length)]

    # Add the boundaries of the workspace
    for i in range(width):
        grid[0][i] = "-"
        grid[length - 1][i] = "-"
    for i in range(length):
        grid[i][0] = "|"
        grid[i][width - 1] = "|"

    # Corners
    grid[0][0]   = "."
    grid[0][-1]  = "."
    grid[-1][0]  = "'"
    grid[-1][-1] = "'"

    # Draw each platform in the workspace
    for item in platform_items:
        platform = next(p for p in platforms if p["name"] == item["platform"])
        size_x, size_y, _ = guess_shape(platform)
        platform_width = round(width_scaler * size_x // downscale_factor)
        platform_length = round(size_y // downscale_factor)
        x = round(width_scaler * item["position"]["x"] // downscale_factor)
        y = round(item["position"]["y"] // downscale_factor)

        # Draw the item as a filled square in the grid
        i_range = range(y, min(y + platform_length, length))
        j_range = range(x, min(x + platform_width, width))
        for i in i_range:
            for j in j_range:
                if platform.get("type") == "ANCHOR" and (i == i_range[0] or j == j_range[0]):
                    # Mark anchor corners.
                    grid[i][j] = anchor_char
                elif grid[i][j] == empty_char:
                    # Mark platform area.
                    grid[i][j] = item_char

    # Convert grid to a string representation
    result = "\n".join([indent * " " + "".join(row) for row in grid])
    return result
