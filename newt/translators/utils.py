"""Utility functions for translations"""

def location_to_position(location: dict, deck_height: float):
    return {
        'x': location['x'],
        # Invert the Y-axis by recalculating the Y coordinate
        'y': deck_height - location['y'],
        'z': location['z']
    }

def scrub(obj:dict, bad_key:str="_remove_this_key"):
    """Remove keys from a dictionary by name.

    Args:
        obj (dict): A dictionary to scrub.
        bad_key (str, optional): Key to remove. Defaults to "_remove_this_key".
    """

    if isinstance(obj, dict):
        # the call to `list` is useless for py2 but makes
        # the code py2/py3 compatible
        for key in list(obj.keys()):
            if key == bad_key:
                del obj[key]
            else:
                scrub(obj[key], bad_key)
    elif isinstance(obj, list):
        for i in reversed(range(len(obj))):
            if obj[i] == bad_key:
                del obj[i]
            else:
                scrub(obj[i], bad_key)
    else:
        # neither a dict nor a list, do nothing
        pass

def get_complete_dimensions_dict(width=None, length=None, height=None, diameter=None):
    result = {
        "width": width if width is not None else 0.0,
        "length": length if length is not None else 0.0,
        "height": height if height is not None else 0.0,
        "diameter": diameter if diameter is not None else 0.0
    }
    return result

def get_dimensions_dict(width=None, length=None, height=None, diameter=None):
    result = {}
    if width is not None:
        result["width"] = width
    if length is not None:
        result["length"] = length
    if height is not None:
        result["height"] = height
    if diameter is not None:
        result["diameter"] = diameter
    return result

def get_dimensions(plr_data: dict):
    if plr_data["shape"] == "circular":
        diameter = plr_data["diameter"]
        height = plr_data['height']
        return get_dimensions_dict(height=height, diameter=diameter)
    else:
        width, length = plr_data["size_x"], plr_data["size_y"]
        height = plr_data['size_z']
        return get_dimensions_dict(width, length, height)

def index_to_row_first_index(c_index: int, rows: int, cols: int) -> int:
    """
    Converts a column-first index from PLR to a row-first index, which traverses
    the matrix starting with items in the first row, then on the second row, and so on.

    Args:
        c_index (int): The index in column-first order (R matrix / PLR style).
        rows (int): The total number of rows.
        cols (int): The total number of columns.

    Returns:
        int: The index in row-first order.
    """
    # Calculate the row and column in column-first order
    row = c_index % rows
    col = c_index // rows

    # Convert to row-first index
    r_index = row * cols + col

    return r_index

def id_to_row_col(id_str: str):
    """
    Converts a row-column ID (like A1, B12, AA4) to its corresponding row and column indexes.

    Args:
        id_str (str): The row-column ID, e.g., 'A1', 'B2', 'AA12'.

    Returns:
        tuple: (row_index, col_index), where row_index is 0-based and col_index is 0-based.

    Example usage:
        id_str = 'AA12'
        row_index, col_index = id_to_row_col(id_str)
        print(f"Row: {row_index}, Column: {col_index}")
    """
    # Separate the letter part (row) and the number part (column)
    row_part = ''.join([char for char in id_str if char.isalpha()])
    col_part = ''.join([char for char in id_str if char.isdigit()])

    # Convert the column part to an integer (0-based)
    col_index = int(col_part) - 1

    # Convert the row part from letters to a number (0-based)
    row_index = 0
    for char in row_part:
        row_index = row_index * 26 + (ord(char.upper()) - ord('A') + 1)

    row_index -= 1  # Convert from 1-based to 0-based index

    return row_index, col_index

def to_single_index(
        row: int, col: int,
        num_columns: int = None, num_rows: int = None,
        row_first = True):
    """
    Converts the row and column index of a 2D grid into a single index for a 1D array.

    Defaults to row-first ordering.

    Args:
        row (int): The row index (0-based).
        col (int): The column index (0-based).
        num_columns (int): The number of columns in the grid.
        num_rows (int): The number of rows in the grid.
        row_first (bool): Select row-first (A1, A2, A3) or col-first (A1, B1, C1) ordering.

    Returns:
        int: The single index in the 1D array.

    Example usage:
        num_columns = 10
        print(to_single_index(0, 0, num_columns))  # Output: 0
        print(to_single_index(1, 2, num_columns))  # Output: 12
        print(to_single_index(3, 5, num_columns))  # Output: 35
    """
    if row_first:
        return row * num_columns + col
    else:
        return col * num_rows + row

def id_to_single_index(
        id_str: str,
        num_columns: int = None, num_rows: int = None,
        row_first = True):
    """
    Converts a row-column ID (e.g., 'A1', 'B3') to a single index in a 1D array,
    given the number of columns in the 2D grid.

    Defaults to row-first ordering.

    This function first translates the row-column ID into numerical row and
    column indexes using the `id_to_row_col` function, then uses the
    `to_single_index` function to compute the corresponding 1D index.

    Args:
        id_str (str): The row-column identifier (e.g., 'A1', 'B3', etc.).
        num_columns (int): The total number of columns in the 2D grid.
        num_rows (int): The number of rows in the grid.
        row_first (bool): Select row-first (A1, A2, A3) or col-first (A1, B1, C1) ordering.

    Returns:
        int: The single index in the 1D array corresponding to the given row-column ID.

    Example:
        Given a grid with 10 columns:
        - 'A1' corresponds to index 0.
        - 'B3' corresponds to index 12.
        - 'D6' corresponds to index 35.

    Example Usage:
        >>> id_to_single_index('A1', 10)
        0
        >>> id_to_single_index('B3', 10)
        12
        >>> id_to_single_index('D6', 10)
        35
    """
    # Get row-col indexes.
    row, col = id_to_row_col(id_str)
    # Get flattened index.
    index = to_single_index(row, col, num_columns, num_rows, row_first)
    return index

def calculate_plr_grid_parameters(plr_data: dict):
    """
    Derives the grid parameters (dx, dy, dz, item_dx, item_dy) from PLR data for an itemized resource.

    Args:
        plr_data (dict): The PLR data containing the deck and its child resources (e.g., wells, tubes).

    Returns:
        dict: A dictionary with the following derived grid parameters:
            - dx: X coordinate of the bottom-left corner of the grid.
            - dy: Y coordinate of the bottom-left corner of the grid (with Y-axis origin at the bottom).
            - dz: Z coordinate for all items (assumed to be constant).
            - item_dx: Distance between adjacent items in the X direction.
            - item_dy: Distance between adjacent items in the Y direction.
    """
    # Extract all child items (e.g., wells or tubes) from the PLR data.
    items = plr_data["children"]

    # Number of columns (items along the X direction) in the grid.
    num_items_x = plr_data["num_items_x"]
    # Number of rows (items along the Y direction) in the grid.
    num_items_y = plr_data["num_items_y"]

    # Sort items by their (x, y) coordinates to align them in grid order.
    sorted_items = sorted(items, key=lambda item: (item["location"]["y"], -item["location"]["x"]), reverse=True)

    # Get the size of the items (assumed to be uniform for all items in this case).
    size_x = sorted_items[0]["size_x"]
    size_y = sorted_items[0]["size_y"]
    # size_z = sorted_items[0]["size_z"]

    # The first item in the sorted list is at the top-left of the grid.
    top_left_item = sorted_items[0]

    # The last item is in the bottom-right of the grid.
    bottom_left_item = sorted_items[(num_items_y - 1) * num_items_x]

    # Derive dx, dy, dz.
    dx = bottom_left_item["location"]["x"]
    dy = bottom_left_item["location"]["y"]
    dz = bottom_left_item["location"]["z"]

    # Get the coordinates of the center.
    dx_center = dx + (size_x / 2)  # Move from bottom-left corner to center.
    dy_center = dy + (size_y / 2)  # Adjust Y accordingly.
    dz_center = dz  #  - (size_z / 2)  # Adjust Z if needed.

    # Derive item_dx: Difference in X between two adjacent items in the same row (i.e., same Y).
    item_dx = sorted_items[1]["location"]["x"] - top_left_item["location"]["x"]

    # Derive item_dy: Difference in Y between two adjacent items in the same column (i.e., same X).
    item_dy = top_left_item["location"]["y"] - sorted_items[num_items_x]["location"]["y"]

    # Return the derived grid parameters.
    return {
        "dx": dx,
        "dy": dy,
        "dz": dz,
        "dx_center": dx_center,
        "dy_center": dy_center,
        "dz_center": dz_center,
        "item_dx": item_dx,
        "item_dy": item_dy,
        "num_items_x": num_items_x,
        "num_items_y": num_items_y,
        "top_left_item": top_left_item,
        "bottom_left_item": bottom_left_item
    }

def derive_grid_parameters_from_plr(plr_data: dict):
    """Derive rack parameters from a PLR itemized resource

    Args:
        plr_data (dict): A serialized PLR itemized resource (e.g. well plate).

    Returns:
        dict:
    """
    # Derive and extract PLR grid parameters.
    params = calculate_plr_grid_parameters(plr_data)
    top_left_item = params["top_left_item"]
    size_x, size_y = top_left_item["size_x"], top_left_item["size_y"]
    # Convert to rack parameters.
    return {
        "wellDiameter": size_x,
        "firstWellCenterX": top_left_item["location"]["x"] + (size_x / 2),
        "firstWellCenterY": (plr_data["size_y"] - top_left_item["location"]["y"]) - (size_y / 2),
        "wellSeparationX": params["item_dx"],
        "wellSeparationY": params["item_dy"],
        "wellsColumns": params["num_items_x"],
        "wellsRows": params["num_items_y"],
    }

def rack_to_plr_dxdy(platform_data, centers=False):
    """Prepare dx, dy, and dz parameters for "create_equally_spaced_2d" given a platform definition.

    Args:
        platform_data (dict): Rack-type platform definition.
        tip_link (dict): Link object between a platform and a calibrated container.
        centers (bool, optional): Calculate dx and dy for the centers instead. Defaults to False.

    Returns:
        tuple: dx, dy, and dz coordinates for the "create_equally_spaced_2d" function.
    """
    # dx: The X coordinate of the bottom left corner for items in the left column.
    dx = platform_data["firstWellCenterX"]
    # Subtract half-diameter to get the X position of its left corner.
    if not centers:
        dx -= platform_data["wellDiameter"]/2

    # dy: The Y coordinate of the bottom left corner for items in the bottom row.
    # NOTE: This must be calculated because PLR's origin is on the bottom-left.
    dy = platform_data["length"]
    dy -= platform_data["firstWellCenterY"]
    dy -= platform_data["wellSeparationY"] * (platform_data["wellsRows"] - 1)
    # Subtract half-diameter to get the Y position of its lower corner.
    if not centers:
        dy -= platform_data["wellDiameter"]/2

    return dx, dy

def calculate_plr_dz_tip(platform_data, container_link):
    """Z coordinate for a tip spot

    NOTE: According to Rick and the sources, the "Z of a TipSpot" is the "Z of the tip's tip" when
    the tip is in its spot, relative to the base of the tip rack (I guessed this last part).
    """
    # Height of the "seat" where the tip "sits" (and touches).
    dz = platform_data["activeHeight"]
    # Subtract tip's length that ends up below the "seat".
    # This is the "z-offset" of a particular container with a particular platform.
    dz -= container_link["containerOffsetZ"]

    return dz

def calculate_plr_dz_tube(platform_data, container_link, container):
    """Z coordinate for a tube spot"""
    # Height of the "seat" where the tube "sits" (and touches).
    dz = platform_data["activeHeight"]
    # Subtract the offset for this particular combination of container and tube rack.
    dz -= container_link["containerOffsetZ"]  # Usually zero.
    # Distance between the tube's external bottom and internal bottom.
    dz += container["activeHeight"]

    return dz

def calculate_plr_dz_slot(platform_data, platform_slot, container_link, container):
    """Z coordinate for a custom spot"""
    # Height of the "seat" where the tube "sits" (and touches).
    dz = platform_slot["slotActiveHeight"]
    # Height offset for all slots in the platform.
    dz += platform_data["activeHeight"]
    # Subtract the offset for this particular combination of container and slot.
    dz -= container_link["containerOffsetZ"]  # Usually zero.
    # Distance between the tube's external bottom and internal bottom.
    dz += container["activeHeight"]

    return dz

def plr_to_xy(x: float, y: float,
              workspace_height: float):
    """Convert XY coordinates from bottom-left origin to top-left origin.

    Inverse function of xy_to_plr, which converts coordinates from the bottom-left origin
    (positive Y upward) back to the top-left origin (positive Y downward).

    - X-coordinate stays the same, since the direction doesn't change in either system.
    - Y-coordinate needs to be inverted by subtracting it from the total workspace height.

    Test:
        foo = (-10, 10)
        bar = deck.xy_to_plr(*foo)
        baz = deck.plr_to_xy(*bar)
        assert foo == baz

    Args:
        x (float): X coordinate in the system with the bottom-left origin.
        y (float): Y coordinate in the system with the bottom-left origin (positive upwards).
        workspace_height (float): Height of the workspace.

    Returns:
        tuple: New (x, y) coordinates in the system with the top-left origin.
    """

    new_x = x  # X stays the same
    new_y = workspace_height - y  # Flip the Y coordinate back to top-left origin

    return new_x, new_y

def xy_to_plr(x: float, y: float,
              workspace_height: float = None):
    """Convert XY coordinates from top-left origin to bottom-left origin.

    To convert XY coordinates from a coordinate system where the origin is at the top-left
    (with the positive X direction to the right and the positive Y direction towards the bottom) to
    another coordinate system where the origin is at the bottom-left, you can apply the following
    conversion:

    - X coordinate stays the same since both systems have the positive X direction to the right.
    - Y coordinate needs to be flipped. In the top-left-origin system, Y increases downward, while
      in the bottom-left-origin system, Y increases upward.

    Given the width and height of the workspace, you can transform the Y-coordinate as follows:

        New Y-coordinate = height_of_workspace - original_y_coordinate

    Args:
        x (float): X coordinate in the original system (top-left origin, positive to the right).
        y (float): Y coordinate in the original system (top-left origin, positive "downwards").
        workspace_height (optional, float): Height of the workspace.

    Returns:
        tuple: New (x, y) coordinates in the system with the bottom-left origin.
    """

    new_x = x  # X stays the same
    new_y = workspace_height - y  # Flip the Y coordinate

    return new_x, new_y
