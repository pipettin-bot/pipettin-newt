"""Functions to convert a PLR Deck to a Pipet Workspace"""
from ..workspaces import base_workspace
from ..platform_items import base_platform_item
from ..platforms import (
    base_platform, platform_anchor, base_grid_platform,
    platform_custom, platform_discard, platform_tip_rack,
    platform_petri, platform_tube_rack
)
from .utils import (
    id_to_row_col,
    to_single_index,
    derive_grid_parameters_from_plr,
    get_dimensions,
    location_to_position
)

def deck_to_workspace(plr_deck, name_suffix = " (PLR export)"):
    """
    Converts a PLR deck data structure to Piper workspace format.

    Args:
        plr_deck (dict): A dictionary representing the deck in PLR format.

    Returns:
        dict: The converted deck in Piper workspace format.
    """
    # Extract relevant fields from the PLR deck
    piper_workspace = base_workspace(
        name = plr_deck['name'] + name_suffix,
        description = f"{plr_deck.get('type', '')} pattern with PLR to Piper conversion.",
        width = plr_deck['size_x'],
        length = plr_deck['size_y'],
        height = plr_deck['size_z'],
        items = [],  # Will need to be populated later when dealing with the items
        padding=plr_deck.get("padding", None)
    )

    return piper_workspace

def convert_item_base(plr_resource, deck_height):
    """ Converts a PLR resource object back to Piper's item format.

    Adjusting the Y-axis and handling the transformation.

    Args:
        plr_resource (dict): A dictionary representing the serialized resource in PLR format.
        deck_height (float): The height of the deck/workspace, used to invert the Y-axis.

    Returns:
        dict: The converted resource in Piper's workspace format,
              with a "platformData" key for importing.
    """

    # TODO: Convert these parameters too.
    # plr_resource['parent_name']
    # plr_resource['rotation']['x']
    # plr_resource['rotation']['y']
    # from pprint import pprint
    # pprint(plr_resource)

    # Platform definition.
    dims = get_dimensions(plr_resource)

    activeHeight = plr_resource.get("active_z", None)
    piper_platform = base_platform(
        name=plr_resource['model'], # Alternatively "type", sometimes.
        platform_type=plr_resource['category'],
        rotation = plr_resource["rotation"]["z"],
        # TODO: Setting this to "0" if missing is  a hack for PLR-first platforms like "Plate".
        activeHeight=activeHeight if activeHeight else 0.0,
        containers=[], slots=[],
        **dims, # width, length, height, diameter
    )

    # Get the position of the item.
    if plr_resource.get('location', None) is not None:
        item_position = location_to_position(plr_resource["location"], deck_height)
    else:
        print(f"WARNING: PLR resource '{plr_resource['name']}' has no location information.")
        item_position = {"x": 0.0, "y": 0.0, "z": 0.0}

    # Item definition.
    locked = plr_resource.get("locked", False)
    piper_item = base_platform_item(
        name=plr_resource['name'],
        platform=plr_resource['model'],  # Alternatively "type", sometimes.
        content=[],
        snappedAnchor=None,
        platformData=piper_platform,
        position=item_position,
        locked=locked if locked else False
    )

    return piper_item, piper_platform, []

def convert_anchor(plr_resource, deck_height):
    # Convert
    piper_item, piper_platform, item_containers = convert_item_base(plr_resource, deck_height)
    # Validate
    assert piper_platform.pop("type") == "ANCHOR"
    # Overrides
    piper_platform = platform_anchor(
        **piper_platform,
        anchorOffsetX = plr_resource["anchor_offset"]["x"],
        anchorOffsetY = plr_resource["anchor_offset"]["y"],
        anchorOffsetZ = plr_resource["anchor_offset"]["z"]
    )
    return piper_item, piper_platform, item_containers

def fix_container_name(content, container, platform_name):
    """Fix a content's name when it is missing.
    This happens with PLR-native wells, tips, and tubes.
    """
    if container["name"] in [None, "default"]:
        content["container"] = platform_name
        container["name"] = platform_name
    return content, container

def plr_content_to_container(plr_content: dict):
    """Convert the serialization of a PLR Tip/Tube object to a container."""
    container = {
        "name": plr_content["model"],
        # TODO: Grab description from somewhere else.
        "description": f"{plr_content['category']} with maximum volume of {plr_content['max_volume']} µL",
        "type": plr_content["category"],  # "tube", "tip", "colony", "well", ...
        "length": plr_content["size_z"],  # Height of the tube
        "maxVolume": plr_content["max_volume"],
        # TODO: "active_z" is only meaningful for PLR objects from
        #       the SilverDeck at the moment. It is not meaningful
        #       for tips or wells, but it is for tubes.
        "activeHeight": plr_content["active_z"]
    }
    return container

def convert_content_base(plr_content: dict, index: int):
    """Generate a base content from a PLR content.
    Position data must be added later (col/row/xyz/...).
    """
    return {
        # TODO: Names must be unique. This should be checked for tips and tubes.
        "name": plr_content.get("name", "content" + str(index)),
        "container": plr_content["model"], # "200 uL Tip Tarsons" (or "default", or None, if from PLR).
        "index": index + 1,
        "tags": plr_content.get("tags", []),
        # TODO: Get the volume from the full "state serialization".
        # "volume": tube["tracker"]["total_volume"]  # Get the total volume in the tube
        "volume": 0.0
    }

def convert_content_slot_xy(spot: dict, plr_content: dict, index: int, parent_height: float):
    """Generate a content based on a slot's XY."""

    container = plr_content_to_container(plr_content)

    content = convert_content_base(plr_content, index)
    content["position"] = {
        "x": spot["location"]["x"],
        "y": parent_height - spot["location"]["y"]
    }

    content, container = fix_container_name(content, container, spot["parent_name"])

    return content, container

def convert_content_row_col(spot: dict, plr_content: dict, index: int, row: int, col: int):
    """Generate a content based on row-col indexes.
    Args:
        spot (dict): Serialized PLR "TipSpot"/"TubeSpot" object.
        plr_content (dict): Serialized PLR "Tip"/"Tube" object.
        index (int): Index number (0-indexed) for the content's index.
        row (int): Index number (1-indexed) for the content's row.
        col (int): Index number (1-indexed) for the content's col.
    """

    container = plr_content_to_container(plr_content)

    content = convert_content_base(plr_content, index)

    content ["position"] = {
        "col": col,
        "row": row
    }

    content, container = fix_container_name(content, container, spot["parent_name"])

    return content, container

def convert_tip(tip_spot: dict, row: int, col: int, plr_resource: dict):
    """Generate a tip content (content_converter implementation)

    Args:
        tip_spot (dict): Serialized PLR "TipSpot" object.
    """

    # Get the tip from the spot's tip tracker, if any.
    tip = tip_spot['tip_tracker'].get("tip", None)

    # Early return if the tip is missing.
    if tip:
        # NOTE: Indexes here are 0-based.
        pip_index = to_single_index(row, col,
                                    num_columns=plr_resource["num_items_x"],
                                    num_rows=plr_resource["num_items_y"],
                                    row_first=True)

        # Generate base content.
        content, container = convert_content_row_col(
            spot=tip_spot, plr_content=tip,
            index=pip_index, row=row + 1, col=col + 1
        )

        # Container overrides.
        # TODO: Add missing container properties.

        return content, container

    return None, None

def convert_well(well: dict, row: int, col: int, plr_resource: dict):
    """Generate a well content (content_converter implementation)
    Wells are a special case in PLR, they are always direct children of the plate.
    That is, with no "well spots".
    """
    # Make the well's index.
    pip_index = to_single_index(row, col,
                                num_columns=plr_resource["num_items_x"],
                                num_rows=plr_resource["num_items_y"],
                                row_first=True)

    # Generate base content.
    content, container = convert_content_row_col(
        well, well,
        pip_index, row + 1, col + 1
    )
    # NOTE: There is no need to override the "name" here, as it is always
    #       required in PLR when a Well is instantiated.

    # Compute total volume.
    liquids =  well['well_tracker'].get("liquids", [])
    content["volume"] = sum(liquid[1] for liquid in liquids) # e.g. liquid: ["WATER", 1000]

    return content, container

def convert_tube(tube_spot: dict, row: int, col: int, plr_resource: dict):
    """Generate a tube content (content_converter implementation)

    Workspace content object:

        {
            "container": "1.5 mL tube",
            "index": 1,
            "name": "ConA",
            "tags": [
                "reagent"
            ],
            "position": {
                "col": 1,
                "row": 1
            },
            "volume": 1200
        }

    Container definition:

        {
            "name": "1.5 mL tube",
            "description": "Standard 1.5 mL microcentrifuge tube with hinged lid",
            "type": "tube",
            "length": 39,
            "maxVolume": 1500,
            "activeHeight": 2.5
        }

    PLR object:
        {
            "name": "buffer",
            "type": "Tube",
            "size_x": 10,
            "size_y": 10,
            "size_z": 39,
            "location": {
                "x": 0,
                "y": 0,
                "z": 0,
                "type": "Coordinate"
            },
            "rotation": {
                "x": 0,
                "y": 0,
                "z": 0,
                "type": "Rotation"
            },
            "category": "tube",
            "model": "1.5 mL tube",
            "children": [
                {
                    "name": "tube-A1",
                    "type": "Tube",
                    "size_x": 10,
                    "size_y": 10,
                    "size_z": 39,
                    "location": {
                        "x": 0,
                        "y": 0,
                        "z": 0,
                        "type": "Coordinate"
                    },
                    "rotation": {
                        "x": 0,
                        "y": 0,
                        "z": 0,
                        "type": "Rotation"
                    },
                    "category": "tube",
                    "model": "1.5 mL tube",
                    "children": [],
                    "parent_name": "Tube Rack [5x16]_tubespot_0_0",
                    "max_volume": 1500,
                    "material_z_thickness": null,
                    "compute_volume_from_height": null,
                    "compute_height_from_volume": null
                }
            ],
            "parent_name": "Tube Rack [5x16]_tubespot_1_0",
            "max_volume": 1500,
            "material_z_thickness": null,
            "compute_volume_from_height": null,
            "compute_height_from_volume": null
            "tube_tracker": {
                "tube": {...}  # NOTE: Maybe the same as the info in [children].
                "tube_state": {
                    "liquids": [
                        [
                            "WATER",
                            1000
                        ]
                    ],
                    "pending_liquids": [
                        [
                            "WATER",
                            1000
                        ]
                    ],
                    "liquid_history": [
                        "WATER"
                    ]
                },
                "pending_tube": {...}  # NOTE: Maybe the same as "pending_tube".
        }

    Args:
        tube_spot (dict): Serialized PLR "TubeSpot" object.
        tube (dict): Serialized PLR "Tube" object.
    """

    # Early return if the tip is missing.
    if tube_spot['children']:

        # Get the tip from the spot.
        tube = tube_spot['children'][0]
        tube_tracker = tube_spot['tube_tracker']

        pip_index = to_single_index(row, col,
                                    num_columns=plr_resource["num_items_x"],
                                    num_rows=plr_resource["num_items_y"],
                                    row_first=True)

        # Generate base content.
        content, container = convert_content_row_col(
            tube_spot, tube,
            pip_index, row + 1, col + 1
        )

        # Compute total volume.
        liquids = tube_tracker["tube_state"].get("liquids", [])
        content["volume"] = sum(liquid[1] for liquid in liquids) # e.g. liquid: ["WATER", 1000]

        # Container overrides.
        # NOTE: There is no need to override the "name" here, as it is always
        #       required in PLR when a Well is instantiated.

        return content, container
    else:
        return None, None

def convert_rack_base(plr_resource, deck_height):
    """Base converter for rack-like resources"""
    # Convert
    piper_item, piper_platform, _ = convert_item_base(plr_resource, deck_height)

    # Get grid parameters.
    rack_params = derive_grid_parameters_from_plr(plr_resource)
    # Add rack params.
    # piper_platform.update(**rack_params)
    piper_platform = base_grid_platform(
        platform_type=piper_platform.pop("type"),
        **piper_platform,
        **rack_params
    )

    # Generate compatible containers.
    compatible_containers, compatible_links = get_compatible_containers(plr_resource)

    return piper_item, piper_platform, compatible_containers, compatible_links

def convert_rack_with(plr_resource, deck_height, content_converter):

    # Convert to base rack object.
    piper_item, piper_platform, compatible_containers, compatible_links = convert_rack_base(
        plr_resource, deck_height)

    # Generate contents.
    content, containers, links = convert_contents_with(plr_resource, content_converter)

    # Add contents.
    piper_item["content"] = content

    # Consolidate container links.
    for link in compatible_links:
        links = consolidate_container_links(link, links)
    # Add container links.
    piper_platform["containers"] = links

    # Unify containers.
    compatible_containers = unify_containers_lists(compatible_containers, containers)

    # Done
    return piper_item, piper_platform, compatible_containers

def consolidate_container_links(container_link, container_links):
    # Look for a matching container link already in the list.
    for link in container_links:
        if container_link == link:
            # Return links as they where.
            return container_links
    # If no matching container was found, append this one.
    container_links.append(container_link)
    return container_links

def unify_containers_lists(compatible_containers, containers):
    """Append a content's container to the list if it is not already in the compatibles list."""
    for container in containers:
        if not container["name"] in [c["name"] for c in compatible_containers]:
            compatible_containers.append(container)
    return compatible_containers

def convert_contents_with(plr_resource, content_converter):
    """Generate contents list out of "spot" resources in the provided resource

    This requires a content converter function, which returns
    a proper Piper content for the provided "plr_resource".

    For example: convert_tip, convert_tube, convert_well, ...
    """
    content = []
    containers = []
    container_links = []

    for row_col_id in plr_resource["ordering"]:

        # Get the resource "spot" by index, from the ordering identifier (A1, A2, ...).
        row, col = id_to_row_col(row_col_id)
        plr_index = to_single_index(row, col,
            num_columns=plr_resource["num_items_x"],
            num_rows=plr_resource["num_items_y"],
            row_first=False)
        # Note that this may be a well instead, as PLR has the as direct children.
        child_spot = plr_resource["children"][plr_index]

        # Generate and insert a content if there is one in the resource spot.
        new_content, new_container = content_converter(child_spot, row, col, plr_resource)
        if new_content:
            # Save the content.
            content.append(new_content)
            # Get the slot's container data.
            container_link_data = {
                "container": new_content["container"],  # Originally "model" in the plr content.
                "containerOffsetZ": child_spot["active_z"]
            }
            # Save it for later, with deduplication.
            container_links = consolidate_container_links(container_link_data, container_links)
        if new_container:
            # Save the container.
            containers.append(new_container)


    return content, containers, container_links

def convert_tip_rack(plr_resource, deck_height):
    # Convert to base rack object.
    piper_item, piper_platform, compatible_containers = convert_rack_with(
        plr_resource, deck_height, convert_tip)
    # Fix tip names.
    if all(c["name"] == "tip" for c in piper_item["content"]):
        # TODO: This is a hack. Native PLR racks don't set unique tip names.
        #       The default name is just "tip". Try making them unique in this case.
        for i, content in enumerate(piper_item["content"]):
            content["name"] += str(content["index"])
            piper_item["content"][i] = content
    # Validate
    assert piper_platform["type"] == "TIP_RACK", f"Wrong type for conversion {piper_platform['type']} -> TIP_RACK"
    # Overrides.
    piper_platform = platform_tip_rack(
        platform_type=piper_platform.pop("type"),
        **piper_platform
    )
    # Done
    return piper_item, piper_platform, compatible_containers

def convert_tube_rack(plr_resource, deck_height):
    """Convert a serialized pipettin-made tube-rack or well-plate resource.
    This works only for resources with "tube spots".
    """
    # Convert to base rack object.
    piper_item, piper_platform, compatible_containers = convert_rack_with(
        plr_resource, deck_height, convert_tube)
    # Validate
    assert piper_platform["type"] == "TUBE_RACK", f"Wrong type for conversion {piper_platform['type']} -> TUBE_RACK"
    piper_platform = platform_tube_rack(
        platform_type=piper_platform.pop("type"),
        **piper_platform
    )
    # Done
    return piper_item, piper_platform, compatible_containers

def convert_well_plate(plr_resource, deck_height):
    """Convert a native PLR well-plate.
    By native I mean a plate-like resource with no "tube spots" in it.
    """
    # Convert to base rack object.
    piper_item, piper_platform, compatible_containers = convert_rack_with(
        plr_resource, deck_height, convert_well)
    # Override "None" Z offsets to zero, not set by PLR.
    # TODO: This is not terrible because Wells have zero offset. Have PLR set them anyway.
    # TODO: Tip racks also don't set this value. Have PLR set it to something reasonable.
    for i, c in enumerate(piper_platform["containers"]):
        if c["containerOffsetZ"] is None:
            piper_platform["containers"][i]["containerOffsetZ"] = 0.0
    # Validate
    assert piper_platform["type"] == "TUBE_RACK", f"Wrong type for conversion {piper_platform['type']} -> TUBE_RACK"
    piper_platform = platform_tube_rack(
        platform_type=piper_platform.pop("type"),
        **piper_platform
    )
    # Done
    return piper_item, piper_platform, compatible_containers

def convert_trash(plr_resource, deck_height):
    """Convert a serialized trash object from PLR back to a Piper BUCKET object

    Piper item:

        {
            "platform": "Tall Trash",
            "name": "Tall Trash",
            "snappedAnchor": null,
            "position": {
                "x": 377.7,
                "y": 84.64,
                "z": 0
            },
            "content": [],
            "locked": false
        },

    PLR item:

        {
            "name": "Tall Trash",
            "type": "Trash",
            "size_x": 100,
            "size_y": 100,
            "size_z": 100,
            "location": {
                "x": 377.7,
                "y": 205.36,
                "z": 0,
                "type": "Coordinate"
            },
            "rotation": {
                "x": 0,
                "y": 0,
                "z": 0,
                "type": "Rotation"
            },
            "category": "BUCKET",
            "model": "Tall Trash",
            "children": [],
            "parent_name": "MK3 Baseplate"
        }
    """
    # Convert to base rack object.
    piper_item, piper_platform, item_containers = convert_item_base(plr_resource, deck_height)
    # Validate
    assert piper_platform["type"] == "BUCKET", "Wrong type for conversion"
    # TODO: Trash-specific overrides.
    piper_platform = platform_discard(
        platform_type = piper_platform.pop("type"),
        **piper_platform
    )
    return piper_item, piper_platform, item_containers

def plr_spot_to_slot(slot: dict, parent_height: float):
    return {
        "slotName": slot["name"],
        "slotPosition": {
            "slotX": slot["location"]["x"],
            # NOTE: The Y coordinate must be flipped back.
            "slotY": parent_height - slot["location"]["y"]
        },
        "slotActiveHeight": slot["active_z"],  # Offset with platform's active height
        "slotSize": slot["size_x"],  # Assuming square slots
        "slotHeight": slot["size_z"],  # Slot's height
        "containers": []  # To be populated if tubes are present
    }

def convert_custom(serialized_custom: dict, deck_height) -> dict:
    """
    Converts a serialized PLR CustomPlatform dictionary back into the JSON format
    for a custom piper platform item.

    Args:
        serialized_custom (dict): The serialized custom platform dictionary to convert.

    Returns:
        dict: A dictionary representing the piper platform JSON.
    """
    # Base properties of the platform
    piper_item, piper_platform, _ = convert_item_base(serialized_custom, deck_height)

    # Generate container definitions, slots, and contents for the resource.
    # TODO: Review if this can be unified with the "content_converter" functions.
    container_definitions, content_list, slots_list = add_contents(serialized_custom)
    piper_item["content"].extend(content_list)
    piper_platform["slots"].extend(slots_list)

    # Custom overrides.
    # piper_platform.update({
    #     # active_z stored during loading
    #     "activeHeight": serialized_custom["active_z"],
    #     # To be populated with the platform's slots
    #     "slots": []
    # })
    # Custom platforms always have a diameter key.
    # piper_platform.setdefault("diameter", 0.0)
    piper_platform = platform_custom(
        platform_type=piper_platform.pop("type"),
        **piper_platform
    )

    # Base properties of the platform item (position and content)
    # piper_item.update({
    #     # Override default, unlock it.
    #     "locked": False
    # })

    # Return the combined platform data
    return piper_item, piper_platform, container_definitions

def convert_colonies(plr_resource: dict):
    content = []
    for i, plr_content in enumerate(plr_resource["children"]):
        colony = convert_content_base(plr_content, i)

        # Set position.
        colony.update({
            "position": {
                "x": plr_content["location"]["x"],
                "y": plr_content["location"]["y"]
            },
            # TODO: This is a hack in "create_petri_dish", because
            #       PLR's Colony has no volume. The "diameter" key
            #       is also set to this value and could be used.
            "volume": plr_content["height"]
        })

        # TODO: There is no colony container in pipettin, this is due.
        #       Delete the "container" key.
        del colony["container"]

        content.append(colony)
    return content

def convert_petri(plr_resource, deck_height):
    # Convert
    piper_item, piper_platform, item_containers = convert_item_base(plr_resource, deck_height)
    # Validate
    assert piper_platform["type"] == "PETRI_DISH"

    # Overrides
    piper_item["content"] = convert_colonies(plr_resource)

    piper_platform = platform_petri(
        platform_type=piper_platform.pop("type"),
        maxVolume = plr_resource["max_volume"],
        **piper_platform
    )
    # piper_platform.update({
    #     "maxVolume": plr_resource["max_volume"],
    # })

    return piper_item, piper_platform, item_containers

def add_contents(plr_data: dict):
    """Generate contents for slots in a serialized resource and append them to an item.

    Args:
        plr_data (dict): _description_
        piper_item (dict): _description_
        piper_platform (dict): _description_

    Returns:
        _type_: _description_
    """

    content_list = []
    slots_list = []
    container_definitions = {}

    # Convert each slot in the custom platform to a piper slot
    slots = plr_data.get("children", [])
    for spot_index, spot in enumerate(slots):
        # Assumes all children are TubeSpot objects
        slot_data = plr_spot_to_slot(spot, plr_data["size_y"])

        # Get the slot's container data.
        container_link_data = make_slot_container_link(spot)
        # Add container to the slot data
        slot_data["containers"].append(container_link_data)

        # Check for any Tube (content) present in the slot
        contents = spot.get("children", [])

        for plr_content in contents:
            # Add the plr_content to the platform's content.
            # Using the index of the child in the children list.
            tube_content, container_definition = convert_content_slot_xy(
                spot, plr_content, spot_index, plr_data["size_y"]
            )

            # Append this content content to the platform's content
            content_list.append(tube_content)

            # Add the container definition if not already present
            if plr_content["model"] not in container_definitions:
                # Use the container model as the unique identifier for deduplication
                container_definitions[plr_content["model"]] = container_definition

        # Append the slot to the platform data
        slots_list.append(slot_data)
        # piper_platform["slots"].append(slot_data)

    # Convert the container definitions dictionary to a list of unique definitions
    unique_container_definitions = list(container_definitions.values())

    return unique_container_definitions, content_list, slots_list

def get_compatible_containers(plr_resource: dict):
    """Retrieve the container definitions for compatible containers
    The compatible list is populated in non-native PLR objects only,
    generated by the SilverDeck.
    """
    containers_list = []
    links_list = []
    compatibles = plr_resource.get("compatibles", [])

    # NOTE: "compat" holds the serialization of a tube/tip and its link to a platform.
    for compat in compatibles:

        # Generate the container definition from the PLR tip/tube object.
        container_definition = plr_content_to_container(compat["content"])

        # TODO: Fix the container's name. ¿Is it needed here?
        # _, container_definition = fix_container_name(
        #     content={}, container=container_definition,
        #     platform_name=plr_resource["name"])

        # Add the container definition
        containers_list.append(container_definition)

        # Make container links.
        # TODO: Now the links are saved to pipettin-plr objects, but
        #       will not be available in others.
        links_list.append(compat["link"])

    return containers_list, links_list

def make_slot_container_link(plr_spot: dict):
    """Generate a container link for a platform's slot.
    The plr_spot can be a TubeSpot, TipSpot, or similar.
    """
    container_data = {
        "container": plr_spot["model"],  # The name of the tube model
        "containerOffsetZ": plr_spot["active_z"] # Offset from slot height
    }
    return container_data

def convert_plr_tip_rack(plr_resource, deck_height):
    """Convert a PLR-native tip rack plate by overriding active_z in wells first.
    The information in PLR is not fully available, so this partially made up.

    We will set active height of the platform and containerOffsetZ to the same value,
    such that piper's calculation still calculates the correct number:

    >>> z += platform_item["position"].get("z", 0)
    >>> z += platform["activeHeight"]             # NOTE: Override to 0.
    >>> z += -container_link["containerOffsetZ"]  # NOTE: Override to 0.
    >>> z += container["activeHeight"]
    """
    # Override the content's "containerOffsetZ".
    for i, _ in enumerate(plr_resource["children"]):
        plr_resource["children"][i]["active_z"] = 99999.0
    # Override the platform's "activeHeight".
    plr_resource["active_z"] = 99999.0
    return convert_tip_rack(plr_resource, deck_height)

# Translator functions for each platform type.
translators = {
    "ANCHOR": convert_anchor,
    "TIP_RACK": convert_tip_rack,
    "TUBE_RACK": convert_tube_rack,
    "BUCKET": convert_trash,
    "CUSTOM": convert_custom,
    "PETRI_DISH": convert_petri,
    "DEFAULT": convert_item_base
}
"""Translators are functions that convert to specific piper objects.

The key here is mapped to the "category" property of a PLR resource.

These will be useful in the case of the SilverDeck, which sets the
category to the type of the platform (e.g. "TIP_RACK").

All "PLR things" will be converted by the default translator, unless
their "category" matches a key in the translators dict.
"""

translate_resource_type_as = {
    "tip_rack": ("TIP_RACK", convert_plr_tip_rack),
    "plate": ("TUBE_RACK", convert_well_plate),
    # "TubeRack": ("TUBE_RACK", translators["TUBE_RACK"]),
}
"""Special case handling
Map a PLR-native "category" to a translator function.
"""

def convert_item(plr_resource, deck_height, snapped_anchor=None):
    """Generate a platform item from a PLR resource"""

    # Get the category of the resource.
    platform_type = plr_resource['category']  # Categories: e.g. "ANCHOR", "TUBE_RACK", "plate" (PLR), "tip_rack (PLR)"...

    # TODO: Do I have a use for class names?
    resource_type = plr_resource['type']      # Class names: e.g. "Plate", "TipRack", ...

    if platform_type in translators:
        # In this case, we are translating an object loaded by a SilverDeck.
        translator = translators.get(platform_type, convert_item_base)
    elif platform_type in translate_resource_type_as:
        # If the PLR category does not match a platform type, try with a special-case translator.
        # Get the new platform type and converter function.
        platform_type, translator  = translate_resource_type_as[platform_type]
        # Override the category in here, to avoid errors in assertion checks above.
        plr_resource['category'] = platform_type
    else:
        print(f"There is no translator for {plr_resource['name']} with category {platform_type} and type {resource_type}. Using the base converter.")
        # Get the default translator function.
        translator = translators["DEFAULT"]

    # Translate.
    piper_item, piper_platform, item_containers = translator(plr_resource, deck_height)

    # Override the snapped anchor if requested.
    if snapped_anchor:
        piper_item["snappedAnchor"] = snapped_anchor

    # Add the container definitions to the item definition.
    piper_item["containerData"] = item_containers

    return piper_item, piper_platform, item_containers

def convert_items(plr_resources, deck_height):
    """Generate platform items from PLR resources

    The "ANCHOR" type is treated specially. Its first child is also converted
    to a platform item, if present.
    """
    # List to hold pairs of items and platforms.
    piper_objects = []

    # Iterate over the resources.
    for plr_resource in plr_resources:

        # Convert the resource and save it.
        piper_objects.append(
            convert_item(plr_resource=plr_resource, deck_height=deck_height)
        )

        # If the resource is an anchor, look for children.
        if plr_resource["category"] == "ANCHOR" and plr_resource.get("children", []):
            # Get the first child.
            plr_anchored_resource = plr_resource["children"][0]
            # Add parent offsets to get the absolute location.
            for a in "xyz":
                plr_anchored_resource["location"][a] += plr_resource["location"][a]
            # Convert it to an item and save it.
            piper_objects.append(
                convert_item(plr_resource=plr_anchored_resource,
                             deck_height=deck_height,
                             snapped_anchor=plr_resource["name"])
            )

    items, platforms, all_containers = [], [], []
    for item, platform, containers in piper_objects:
        # Save all items.
        # TODO: Check for name duplication.
        items.append(item)

        # Save platforms, deduplicating by name.
        if platform["name"] not in [p["name"] for p in platforms]:
            platforms.append(platform) # Unique list

        # Save containers, deduplicating.
        if containers:
            dupes = [c for c in all_containers if c["name"] == containers[0]["name"]]
            if not dupes:
                all_containers.extend(containers)
            elif containers[0] != dupes[0]:
                raise ValueError(
                    f"Container with name {containers[0]['name']} is duplicated, but its properties differ."
                )

    return items, platforms, all_containers

def deck_to_workspaces(serialized_deck) -> list:
    """Convert a PLR deck to a Workspace and its items
    The output can be imported to the UI directly.
    """
    # Get resources and deck height.
    resources = serialized_deck["children"]
    deck_height = serialized_deck["size_y"]
    # Generate workspace.
    new_ws = deck_to_workspace(serialized_deck)
    # Generate items and contents.
    new_items, new_platforms, new_containers = convert_items(resources, deck_height)
    # Build workspaces list.
    new_ws["items"] = new_items
    new_workspaces = [new_ws]
    return new_workspaces, new_items, new_platforms, new_containers

def deck_to_db(serialized_deck: dict) -> dict:
    result = deck_to_workspaces(serialized_deck)
    return {
        "workspaces": result[0],
        "items": result[1],
        "platforms": result[2],
        "containers": result[3]
    }
