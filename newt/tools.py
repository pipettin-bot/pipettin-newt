# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate
from .schemas.tools import base_tool_schema

def base_tool(name: str,
              tool_type: str,
              description: str,
              parameters: dict,
              platforms: list = None,
              platformTypes: list = None,
              stepTypes: list = None):
    # Defaults.
    if platforms is None:
        platforms = []
    if platformTypes is None:
        platformTypes = []
    if stepTypes is None:
        stepTypes = []
    # Construct.
    tool = {
        "name": name,
        "type": tool_type,
        "description": description,
        "parameters": parameters,
        "platforms": platforms,
        "platformTypes": platformTypes,
        "stepTypes": stepTypes
    }
    # Validate.
    validate(tool, base_tool_schema)

    return deepcopy(tool)

# Examples:
# base_tool()
