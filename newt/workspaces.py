# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from .schemas.workspaces import validate_workspace

def base_workspace(name: str,
                   width: float,
                   length: float,
                   height: float,
                   items: list,
                   thumbnail: str = None,
                   padding: dict = None,
                   description: str = "Bare-bones workspace data."):
    """Basic workspace generator.

    Creates and validates a workspace data object.

    Args:
        name (str): See schema.
        width (float): See schema.
        length (float): See schema.
        height (float): See schema.
        padding (dict): See schema. Example: {"top": 0, "bottom": 0, "left": 0, "right": 0}
        description (str): See schema.
        items (list): See schema.
        thumbnail (str): See schema.

    Returns:
        dict: A deep copy of all properties.
    """
    if padding is None:
        padding = {"top": 0, "bottom": 0, "left": 0, "right": 0}
    if thumbnail is None:
        thumbnail = ""
    data = {
        "name": name,
        "width": width,
        "length": length,
        "height": height,
        "padding": padding,
        "description": description,
        "items": items,
        "thumbnail": thumbnail
    }

    validate_workspace(data)

    return deepcopy(data)
