# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from jsonschema import validate
from . import contents
from . import platforms
from . import containers
#### JSON schemas for platform items ####

base_item_schema = {
    "title": "Platform item.",
    "description": "Instance of a platform placed on a particular workspace.",
    "type" : "object",
    "properties" : {
        "platform":  {"type" : "string", "description": "ID of the platform definition."},
        "name":      {"type" : "string", "description": "Unique name of this instance on the workspace."},
        "position":  {
            "type" : "object",
            "description": "Position of the platform relative to the workspace (may be overridden by a slot if it anchors this platform).",
            "oneOf": [contents.base_coordinate_xyz_schema,
                      contents.base_coordinate_colrow_schema]
        },
        "content": {
            "type": "array",
            "items": {"oneOf": [contents.base_content_schema,
                                contents.base_content_schema_coords]},
            "uniqueItems": True
        },
        "snappedAnchor": {
            "type": ["string", "null"],
            "description": "Helper property to indicate the anchor item to which this item snapped last."
        },
        "locked": {"type": "boolean"},
        "platformData": platforms.base_platform_schema,
        # NOTE: Skipping full validation of "platformData" because it complicates things.
        # "description": "Platform definition for this item. Present in data from exported workspaces.",
        # "platformData": platforms.platforms_schema
        "containerData": containers.base_container_schema,
    },
    "required": [
        "platform",
        "name",
        "position",
        "content",
        "snappedAnchor",
        "locked"
    ]
}


def validate_item(item):
    validate(item, schema=base_item_schema)
