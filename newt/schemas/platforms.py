# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate

# Internal imports
# from . import contents
# from ..utils import update_optional

# CONTAINER LINK SCHEMA ####
calibrated_container_schema = {
    "type": "object",
    "description": "Defines offsets relative to a compatible container.",
    "properties": {
        "container": {"type": "string", "description": "Unique ID of a container."},
        "containerOffsetZ": {
            "type": "number",
            "description": "Offset subtracted for height calculations."
        }
    },
    "required": ["container", "containerOffsetZ"]
}

# SLOT SCHEMAS ####

calibrated_slot_schema = {
    "type": "object",
    "description": "Defines offsets and containers of a 'slot' in a custom platform.",
    "properties": {
        "slotName": {"type": "string"},
        "slotPosition": {
            "type": "object",
            "properties": {
                "slotX": {"type": "number"},
                "slotY": {"type": "number"}
            },
            "required": ["slotX", "slotY"]
        },
        "slotActiveHeight": {"type": "number"},
        "slotSize": {"type": "number", "exclusiveMinimum": 0},
        "slotHeight": {"type": "number", "exclusiveMinimum": 0},
        "containers": {
            "type": "array",
            "items": calibrated_container_schema,
            "uniqueItems": True
        },
        # "id": {"type": "string", "description": "Unique ID identifying each slot."},
        # "offset": deepcopy(contents.base_coordinate_xyz_schema)
    },
    "required": [
        "slotName",
        "slotPosition",
        "slotActiveHeight",
        "slotSize",
        "slotHeight",
        "containers"
    ]
}
# {
#   "slotName": "Well1/48",
#   "slotPosition": {
#     "slotX": 14.38,
#     "slotY": 11.24
#   },
#   "slotActiveHeight": 0,
#   "slotSize": 9,
#   "slotHeight": 10,
#   "containers": [
#     {
#       "container": "Well #96",
#       "containerOffsetZ": 0
#     }
#   ]
# }



# GENERIC PLATFORM SCHEMA ####
platform_types = ["BUCKET", "TIP_RACK", "TUBE_RACK", "PETRI_DISH", "CUSTOM", "ANCHOR"]
base_platform_schema = {
    # "$schema": "https://json-schema.org/draft/2020-12/schema",
    # "$id": "https://example.com/product.schema.json",
    "title": "Platform object.",
    "description": "A 'platform' represents a physical object which holds stuff in place (e.g. tips), which are referred to as 'contents'.",
    "type" : "object",
    "properties" : {
        # Generic drawing parameters.
        "name": {"type" : "string"},
        "description": {"type" : "string"},
        "type": {
            "type" : "string",
            "description": "Defines the 'type' of a platform. This info is used by the GUI to know how to draw them.",
            "enum": platform_types
        },
        "color": {"type" : "string"},
        # Generic spatial parameters.
        "width": {"type": "number", "minimum": 0},
        "length": {"type": "number", "minimum": 0},
        "height": {"type": "number", "exclusiveMinimum": 0},
        "diameter": {"type": "number", "minimum": 0},
        "activeHeight": {"type": "number"},
        "rotation": {"type": "number", "multipleOf" : 90, "description": "Degrees of rotation on the XY plane."},
        # Calibrated platform-slots pairs, as discussed here:
        # https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/100#note_1489622643
        "slots": {
            "type": "array",
            "items": calibrated_slot_schema,
            "uniqueItems": True
        },
        # Calibrated containers.
        "containers": {
            "type": "array",
            "items": calibrated_container_schema,
            "uniqueItems": True
        },
        # New, not implemented in the GUI yet.
        # "graphics": {
        #     "type": "string",
        #     "description": "An SVG that will be drawn on the workspace, overriding the default for the platform 'type'."
        # }
    },
    "required": [ "name", "description", "type", "color", "rotation", "height", "activeHeight" ],
    "additionalProperties": False
}

# ANCHOR ####
platform_schema_anchor = deepcopy(base_platform_schema)  # No additional properties.
platform_schema_anchor["properties"].update({
    "type": {"const": "ANCHOR"},
    # Anchor parameters.
    "anchorOffsetX": {"type": "number"},
    "anchorOffsetY": {"type": "number"},
    "anchorOffsetZ": {"type": "number"},
    # Generic spatial parameters.
    "width": {"type": "number", "exclusiveMinimum": 0},
    "length": {"type": "number", "exclusiveMinimum": 0},
    "diameter": {"const": 0},
    # No slots or containers allowed.
    "slots": {"type": "array", "maxItems": 0},
    "containers": {"type": "array", "maxItems": 0},
})
platform_schema_anchor["required"].extend([
    "width", "length",
    "anchorOffsetX",
    "anchorOffsetY",
    "anchorOffsetZ"
])

# TRASH BUCKET ####
platform_schema_bucket = deepcopy(base_platform_schema)  # No additional properties.
platform_schema_bucket["properties"].update({
    "type": {"const": "BUCKET"},
    # Generic spatial parameters.
    "width": {"type": "number", "exclusiveMinimum": 0},
    "length": {"type": "number", "exclusiveMinimum": 0},
    "diameter": {"const": 0},
    # No slots or containers allowed.
    "slots": {"type": "array", "maxItems": 0},
    "containers": {"type": "array", "maxItems": 0},
})
platform_schema_bucket["required"].extend(["width", "length"])

# PETRI DISH ####
platform_schema_petri = deepcopy(base_platform_schema)  # No additional properties.
platform_schema_petri["properties"].update({
    # Type parameter.
    "type": {"const": "PETRI_DISH"},
    # Petri spatial parameters.
    "diameter": {"type": "number", "exclusiveMinimum": 0},
    "width": {"const": 0},
    "length": {"const": 0},
    # Petri dish parameters
    "maxVolume": {"type": "number", "exclusiveMinimum": 0},
    # No slots or containers allowed.
    "slots": {"type": "array", "maxItems": 0},
    "containers": {"type": "array", "maxItems": 0},
})
platform_schema_petri["required"].extend(["diameter", "maxVolume"])

# GENERIC GRID-TYPE PLATFORM ####
platform_schema_grid = deepcopy(base_platform_schema)
platform_schema_grid["properties"].update({
    # Type parameter.
    "type": {"type" : "string", "enum": ["TIP_RACK", "TUBE_RACK"]},
    # Generic spatial parameters.
    "width": {"type": "number", "exclusiveMinimum": 0},
    "length": {"type": "number", "exclusiveMinimum": 0},
    # 2D-grid drawing and location parameters.
    "firstWellCenterX": {"type": "number", "exclusiveMinimum": 0},
    "firstWellCenterY": {"type": "number", "exclusiveMinimum": 0},
    "wellDiameter": {"type": "number", "exclusiveMinimum": 0},
    "wellSeparationX": {"type": "number", "exclusiveMinimum": 0},
    "wellSeparationY": {"type": "number", "exclusiveMinimum": 0},
    "wellsColumns": {"type": "integer", "exclusiveMinimum": 0},
    "wellsRows": {"type": "integer", "exclusiveMinimum": 0},
    # No slots allowed.
    "slots": {"type": "array", "maxItems": 0},
})
platform_schema_grid["required"].extend([
    "width", "length",
    "containers",
    "firstWellCenterX",
    "firstWellCenterY",
    "wellDiameter",
    "wellSeparationX",
    "wellSeparationY",
    "wellsColumns",
    "wellsRows",
])

# TIP RACK ####
platform_schema_tip_rack = deepcopy(platform_schema_grid)
platform_schema_tip_rack["properties"].update({
    # Type parameter.
    "type": {"const" : "TIP_RACK"},
})

# TUBE RACK ####
platform_schema_tube_rack = deepcopy(platform_schema_grid)
platform_schema_tube_rack["properties"].update({
    # Type parameter.
    "type": {"const" : "TUBE_RACK"},
})


# CUSTOM PLATFORMS ####

# Rectangular
platform_schema_custom_rectangular = deepcopy(base_platform_schema)
platform_schema_custom_rectangular["description"] = "Rectangular custom platform."
platform_schema_custom_rectangular["properties"].update({
    "type": {"const": "CUSTOM"},
    # No containers allowed.
    "containers": {"type": "array", "maxItems": 0},
    # Generic spatial parameters.
    "width": {"type": "number", "exclusiveMinimum": 0},
    "length": {"type": "number", "exclusiveMinimum": 0},
    "diameter": {"const": 0}
})
platform_schema_custom_rectangular["required"].extend(["width", "length", "slots"])

# Circular
platform_schema_custom_round = deepcopy(base_platform_schema)
platform_schema_custom_round["description"] = "Circular custom platform."
platform_schema_custom_round["properties"].update({
    "type": {"const": "CUSTOM"},
    # No containers allowed.
    "containers": {"type": "array", "maxItems": 0},
    # Petri spatial parameters.
    "diameter": {"type": "number", "exclusiveMinimum": 0},
    "width":  {"const": 0},
    "length":  {"const": 0},
})
platform_schema_custom_round["required"].extend(["diameter", "slots"])

# Custom (any)
platform_schema_custom = {
    "title": "Custom platform with slots in arbitrary locations",
    "type": "object",
    "oneOf": [
        platform_schema_custom_round,
        platform_schema_custom_rectangular
    ]
}

# GENERAL SCHEMA ####
platforms_schema = {
    "title": "Schema for all platforms",
    "type": "object",
    "oneOf": [
        platform_schema_bucket,
        platform_schema_petri,
        platform_schema_tip_rack,
        platform_schema_tube_rack,
        platform_schema_custom,
        platform_schema_anchor
    ]
}

def validate_platform(platform):
    validate(platform, platforms_schema)

# if __name__ == "__main__":
#     from pprint import pprint, pformat
#     # pprint(platform_schema_tip_rack)
#     validate(EXAMPLE_PLATFORM_TIP_RACK_MULTITOOOL, platform_schema_tip_rack)
#
#     # pprint(platform_schema_petri)
#     validate(EXAMPLE_PLATFORM_PETRI_DISH, platform_schema_petri)
#
#     # pprint(platform_schema_bucket)
#     validate(EXAMPLE_PLATFORM_TRASH_BUCKET, platform_schema_bucket)
#
#     # pprint(platform_schema_tube_rack)
#     validate(EXAMPLE_PLATFORM_TUBE_RACK, platform_schema_tube_rack)
#
#     print("Validated all example platforms with their schemas :)")
