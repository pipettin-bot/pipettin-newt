# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from jsonschema import validate
from . import platform_items

# BASE WORKSPACE SCHEMA ####

workspace_padding_schema = {
    "title": "Padding coordinates.",
    "description": "Coordinates for the padding of a workspace.",
    "type": "object",
    "properties": {
        "top": {"type": "number"},
        "bottom": {"type": "number"},
        "left": {"type": "number"},
        "right": {"type": "number"}
    }
}

base_workspace_schema = {
   "title": "Workspace.",
   "description": "A workspace represents a particular configuration of lab objects and their contents, which can run certain protocols.",
   "type" : "object",
   "properties" : {
        "name":      {"type" : "string", 
                      "description": "Unique name for the workspace."},
        "width": {"type": "number"},
        "length": {"type": "number"},
        "height": {"type": "number"},
        "padding": workspace_padding_schema,
        "description": {"type" : "string", 
                        "description": "A few workds describing this workspace and its protocols."},
        "items": {
            "type": "array",
            "description": "Platform items in the workspace.",
            "items": {"anyOf": [platform_items.base_item_schema]},
            "uniqueItems": True
        },
        "thumbnail": {"type": "string"}
   }
}

def validate_workspace(workspace):
    validate(workspace, schema=base_workspace_schema)
