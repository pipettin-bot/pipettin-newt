# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from .platforms import platform_types
from .hl_protocols import step_schemas_dict

#### JSON schemas for a pipette tool ####

tool_types = ["Micropipette", "syringe", "picker", "pinner", "camera", "probe", "laser"]

base_tool_schema = {
    "title": "Tools for the pipettin bot.",
    "description": "A 'tool' is a piece of equipment that can be moved around and used by the robot.",
    "type" : "object",
    "properties" : {
        "name": {"type" : "string"},
        "description": {"type" : "string"},
        "type": {
            "type": "string",
            "description": "Type of tool.",
            # NOTE: Only micropipettes work for now.
            "enum": tool_types},
        "parameters":  {
            "type": "object",
            "description": "Parameters needed to use the tool as intended. These are unrestricted."
        },
        "platforms": {
            "type": "array",
            "description": "IDs of platforms compatible with the tool.",
            "items": {"type": "string"}}, 
        "platformTypes": {
            "type": "array",
            "description": "Types of platforms compatible with the tool.",
            "items": {"type": "string", "enum": platform_types}}, 
        "stepTypes": {
            "type": "array",
            "description": "Types of steps compatible with the tool.",
            "items": {"type": "string", "enum": list(step_schemas_dict)}}
    },
    "required": [
        "name",
        "description",
        "type",
        "parameters",
        "platforms",
        "platformTypes",
        "stepTypes"
    ],
    "additionalProperties": False
}


#### Validation examples ####

# test_instance = EXAMPLE_PLATFORM_ITEM
# reference_schema = base_content_schema_coords
# validate(instance=test_instance, schema=reference_schema)
