# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate

#### JSON schemas for contents ####

base_container_schema = {
    "title": "Minimal container data.",
    "description": "Containers are abstractions of little recipients able to hold some content, usually a liquid.",
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "description": {"type": "string"},
        "type": {"type": "string",
                 "enum": ["tube", "tip", "well", "colony"]},
        "length": {"type": "number"},
        "maxVolume": {"type": "number"},
        "activeHeight": {"type": "number"}
    },
    "required": ["name", "type", "length", "maxVolume", "activeHeight"]
}

def validate_container(container):
    validate(container, base_container_schema)
