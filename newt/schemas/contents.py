# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from jsonschema import validate

#### JSON schemas for contents ####

base_coordinate_xyz_schema = {
    "title": "Position coordinate object, platformless.",
    "description": "Specifies the location of a content in terms of an offset to the platform's origin.",
    "type" : "object",
    "properties": {
        "x": {"type": "number"},
        "y": {"type": "number"},
        "z": {"type": "number"}
    },
    "additionalProperties": False,
    "required": [ "x", "y", "z" ]
}

base_coordinate_colrow_schema = {
    "title": "Coordinate object based on column-row pairs, dependent on platform parameters for absolute position.",
    "description": "Specifies the location of a content in terms of the columns and rows of a grid-type platform.",
    "type" : "object",
    "properties": {
        "col": {"type": "integer"},
        "row": {"type": "integer"}
    },
    "additionalProperties": False,
    "required": [ "col", "row" ]
}

container_type_schema = {
    "type" : "string", 
    "description": "Defines the 'container' associated to a particular content."
}

content_index_shcema = {"type" : "integer",
                        "description": "Unique number identifying the content in the platform, also serves as location index for grid-type platforms.",
                        "minimum": 0}

content_tags_schema = {"type" : "array",
                       "description": "List of 'tags' for a particular content.",
                       "items": {"type": "string"},
                       "uniqueItems": True}

base_content_schema = {
   # "$schema": "https://json-schema.org/draft/2020-12/schema",
   # "$id": "https://example.com/product.schema.json",
   "title": "Platform content with col-row or xyz position.",
   "description": "Tubes, tips, or wells, belonging to a certain platform.",
   "type" : "object",
   "properties" : {
        "container": container_type_schema,
        "index":     content_index_shcema,
        "name":      {"type" : "string"},
        # NOTE: "oneOf" forces a property to be one of the options in an array.
        "position":  {"type" : "object",
                      "oneOf": [base_coordinate_colrow_schema, base_coordinate_xyz_schema]},
        "tags":      content_tags_schema,
        "volume":    {"type" : "number"}
   },
   "required": [ "container", "position" ],
   "additionalProperties": True
}

def validate_content(content):
    validate(content, base_content_schema)

base_content_schema_coords = {
   # "$schema": "https://json-schema.org/draft/2020-12/schema",
   # "$id": "https://example.com/product.schema.json",
   "title": "Content with a XYZ position.",
   "description": "Platformless definition of a single content, using XYZ coordinates.",
   "type" : "object",
   "properties" : {
        # NOTE: "enum" restricts the possible values that a property can have.
        "container": container_type_schema,
        "index":     content_index_shcema,
        "name":      {"type" : "string"},
        # NOTE: "oneOf" forces a property to be one of the options in an array.
        "coords":    {"type" : "object",
                      "oneOf": [base_coordinate_xyz_schema]},
        "tags":      content_tags_schema,
        "volume":    {"type": "number"},
   },
   "required": [ "container", "coords" ],
   "additionalProperties": True
}

# Validation examples:
# test_instance = {"type" : "tip", 
#                  "coords" : {"x": 10.1, "y": 10.9, "z": 10.2},
#                  "maxVolume": 160,
#                  "tipLength": 50.0,
#                  "volume": 0}
# validate(instance=test_instance, schema=base_content_schema_coords)
# validate(instance={"type" : "tip", "position" : {"col": 1, "row": 11}}, schema=base_content_schema)
