# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate

# EXAMPLE MID-LEVEL/ACTIONS PROTOCOL OBJECT ####

action_schema = {
  "type": "object",
  "properties": {
    "cmd": {"type": "string"},
    "args": {"type": "object"},
    "stepID": {"type": "string", 
               "description": "Identifier for the high-level step this action is part of."}
  },
  "required": ["cmd", "args"]
}

actions_protocol_schema = {
  "type": "object",
  "properties": {
    "name": {"type": "string"},
    "description": {"type": "string"},
    "meta": {"type": "string"},
    "hLprotocol": {"type": "string"},
    "workspace": {"type": "string"},
    "actions": {
      "type": "array",
      "items": action_schema
    }
  },
  "required": ["name", "hLprotocol", "actions"]
}

def validate_protocol(protocol):
    validate(protocol, schema=actions_protocol_schema)
