# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from jsonschema import validate
from . import contents

# High-level transfer step ####

# New models as discussed:
# https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/104#note_1489638114
used_contents_schema = {
    "type": "array",
    "description": "Contents used by the step.",
    "items": {"oneOf": [contents.base_content_schema,
                        contents.base_content_schema_coords]},
    "uniqueItems": True
}

source_platform_schema = {
    "type": "object",
    "name": "source",
    "description": "Source platform selector.",
    "properties": {
        "item": {
          "type": "string",
          "description": "Name of the platform (can be 'any')."}, # "5x16_1.5_rack 1"
        "by": {
          "type": ["integer", "string"],
          "enum": ['name', 'tag', 'index'],
          "description": "Defines how the platform content is going to be selected"}, # "name"
        "value": {
          "type": "string",
          "description": "name of the content or tag"}, # "loading_buffer_5x"
        "treatAs": {
          "type": "string",
          "description": "Wether to treat all like unique sources ('same'), or iterate this step for each source ('for_each').",
          "enum": ['same', 'for_each']}, # "same"
        # New properties as discussed:
        # https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/104#note_1489638114
        "contents": used_contents_schema
    },
    "required": ["item", "by", "value", "treatAs"]
}

targets_platform_schema = {
    "type": "object",
    "name": "target",
    "description": "Source platform selector.",
    "properties": {
        "item": {
            "type": "string",
            "description": "Name of the platform (can be 'any')."}, # "5x16_1.5_rack 1"
        "by": {
            "type": ["integer", "string"],
            "description": "defines how the platform content is going to be selected",
            "enum": ['name', 'tag', 'index']}, # "tag"
        "value": {
            "type": "string",
            "description": "name of the content or tag"}, # "sample"
        # New properties as discussed:
        # https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/104#note_1489638114
        "contents": used_contents_schema
    },
    "required": ["item", "by", "value"]
}

tip_schema = {
    "type": "object",
    "properties": {
        "mode": {
            "type": "string",
            "description": "???",
            "enum": ['reuse', 'isolated_source_only', 'isolated_targets', 'reuse_same_source']}, # "reuse"
        "item": {
            "type": "string",
            "description": "name of tip rack"}, # "200ul_tip_rack_MULTITOOL 1"
        "discardItem": {
            "type": "string",
            "description": "name of bucket"} # "trash_bucket 1"
    },
    "required": ["mode", "item", "discardItem"]
}

tool_schema = {
    "type": "string", # e.g. P20
    "description": "ID of the tool to use for this volume transfer (can be 'auto')."
}

volume_schema = {
    "type": "object",
    "description": "Defines how much volume is transferred and how.",
    "properties": {
        "value": {"type": "number"}, # e.g. 10
        "type": {
            "type": "string",
            "description": "Define how to interpret volume relative to the targets.",
            "enum": [
                # Transfer "volume" to each target.
                'fixed_each',
                # Transfer "volume" in total, split among targets.
                'fixed_total'
        ]},
        # New properties as discussed:
        # https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/104#note_1489638114
        "count": {
            "type": "integer",
            "description": ""
        }
    },
    "required": ["value", "type"]
}

volume_schema_tag = {
    "type": "object",
    "description": "Defines how much volume is transferred and how.",
    "properties": {
        "value": {"type": "number"}, # e.g. 10
        "type": {
            "type": "string",
            "description": "Define how to interpret volume relative to the targets.",
            "enum": [
                # Multiply "volume" by the amount of contents with a certain "tag",
                # and transfer that amount to each tube.
                'for_each_target_tag'
        ]},
        "tag": {
            "type": "string",
            "description": "Name of the tag used to count contents (only if 'for_each_target_tag' type is chosen)"
        },
        # New properties as discussed:
        # https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/104#note_1489638114
        "count": {
            "type": "integer",
            "description": "Amount of contents detected by the selector."
        }
    },
    "required": ["value", "type", "tag"]
}

step_transfer_schema = {
    "type": "object",
    "description": "A liquid transfer step.",
    "properties": {
        "source": source_platform_schema,
        "target": targets_platform_schema,
        "volume": {
            "type" : "object",
            "oneOf": [volume_schema, volume_schema_tag]
        },
        "tip": tip_schema,
        "tool": tool_schema
    },
    "required": ["source","target","volume", "tip", "tool"]
}

# High-level Mix Step ####

step_mix_schema = {
    "type": "object",
    "description": "A liquid homogenization step (mixed, not stirred).",
    "properties": {
        "target": targets_platform_schema,
        "mix": {
            "type": "object",
            "description": "Defines how much volume is used for mixing.",
            "properties": {
                "type": {"type": "string",
                         "description": "???",
                         "enum": [
                            # ???
                            'content'
                         ]},
                "percentage": {"type": "number"},
                "count": {"type": "integer"}
            }
        },
        "tip": tip_schema,
        "tool": tool_schema
    },
    "required": ["target","mix","tip","tool"]
}

# High-level Human Step ####

step_human_schema = {
    "type": "object",
    "description": "Tells a human to do something before carrying out the next step.",
    "properties": {
        "text": {"type": "string"}
    },
    "required": ["text"]
}

# High-level Wait Step ####

step_wait_schema = {
    "type": "object",
    "description": "Wait for the specified amount of time in seconds, before carrying out the next step.",
    "properties": {
        "seconds": {"type": "number"}
    },
    "required": ["seconds"]
}

# High-level Comment Step ####

step_comment_schema = {
    "type": "object",
    "description": "A comment step, to add context for humans. Ignored everywhere but kept.",
    "properties": {
        "text": {"type": "string"}
    },
    "required": ["text"]
}

# High-level step and protocol ####

# Dict of step schemas and their command names.
step_schemas_dict = {
    # Auxiliary list of steps and their types.
    "TRANSFER": step_transfer_schema,
    "WAIT": step_wait_schema,
    "HUMAN": step_human_schema,
    "COMMENT": step_comment_schema,
    "MIX": step_mix_schema,
}
# Helper HL-step schema template.
def _make_hl_step_schema(step_command: str, step_definition: dict):
    return {
        "type": "object",
        "properties": {
            "order": {"type": "number"},
            "name": {"type": "string"},
            "type": {"type": "string", "const": step_command},
            "definition": step_definition
        },
        "required": ["order", "name", "type", "definition"]
    }
# Helper dict of schemas.
_hl_step_schema_dict = {c: _make_hl_step_schema(c, d) for c, d in step_schemas_dict.items()}

# High-level step schema.
hl_step_schema = {
    "type": "object",
    "oneOf": list(_hl_step_schema_dict.values())
}
hl_step_sequence_schema = {
    "description": "High-level steps for the protocol",
    "type": "array",
    "items": hl_step_schema
}
# High-level protocol schema.
hl_protocol_schema = {
    "title": "High-level protocol schema.",
    "description": "A high-level protocol is a sequence of high-level steps," + \
        "associated with a particular workspace and its labware (a.k.a. platform items)." + \
        "A step can represent 'transfer N amount from X to Y', and involves multiple actions.",
    "type": "object",
    "properties": {
        "name": {"type": "string", "description": "A simple name for the protocol"},
        "description": {"type": "string"},
        "workspace": {"type": "string"},
        "template": {"type": "string"},
        "templateDefinition": {"type": "object"},
        "templateFields": {"type": "object"},
        # Define the steps object as an array of "high-level step" objects.
        "steps": hl_step_sequence_schema
    },
    "required": [ "name", "workspace", "steps" ]
    # "timestamps": true, # Not a JSON-Schema keyword.
    # "strictQuery": true  # Not a JSON-Schema keyword.
}

def validate_hl_protocol(hl_protocol):
    validate(hl_protocol, schema=hl_protocol_schema)

def validate_hl_step(hl_step):
    validate(hl_step, schema=hl_step_schema)

# Validation examples, not run.
# if __name__ == "__main__":
#   import json
#   from copy import deepcopy
#   test_instance = EXAMPLE_HL_PROTOCOL
#   reference_schema = hl_protocol_schema
#
#   try:
#     validate(instance=test_instance, schema=reference_schema)
#   except Exception as e:
#     print(e)
#   else:
#     print("Success!")
#
#   with open('models/hl_protocols.json', 'w', encoding='utf-8') as f:
#     json.dump(reference_schema, f, ensure_ascii=False, indent=2)
#
#   with open('models/hl_protocols-example.json', 'w', encoding='utf-8') as f:
#     json.dump(test_instance, f, ensure_ascii=False, indent=2)
