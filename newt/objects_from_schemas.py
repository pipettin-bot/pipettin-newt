# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

class O:
  """A hacky class to make functions from schema objects.

  Example:
    from hl_protocols import source_platform_schema, targets_platform_schema, step_transfer_schema
    object = O([source_platform_schema, targets_platform_schema, step_transfer_schema])
    object.transfer()
  """
  def __init__(self, schemas, verbose=True):
    self.verbose=verbose
    if not isinstance(schemas, list):
       schema_list = [schemas]
    else:
       schema_list = schemas
    for schema in schema_list:
      self.__add_generator_method(schema)
  def __add_generator_method(self, schema):
    type_map={"string": "str", "number": "float", "object": "O"}
    f_args = ', '.join([prop_name + ": " + type_map[prop["type"]] + " = None" for prop_name, prop in schema["properties"].items() ])
    f_name = schema["name"]
    f_dict = "{" + ', '.join([ f"'{prop_name}': {prop_name}" for prop_name in schema["properties"].keys() ]) + "}"
    f_code = f"""def {f_name}({f_args}):
      return {f_dict}
    """
    if self.verbose:
      print(f_code)
    exec(f_code)
    setattr(self, f_name, eval(f_name)) 
