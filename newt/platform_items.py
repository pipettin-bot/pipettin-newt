# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from .schemas.platform_items import validate_item

def base_platform_item(
    platform: str,
    name: str,
    position: dict,
    locked: bool = False,
    snappedAnchor: str = None,
    content: list = None,
    platformData: dict = None,
    containerData: dict = None):
    """Basic platform item generator.

    Args:
        platform (str, optional): Platform ID for the item. Example: "Axygen p200 tip rack"
        name (str, optional): Name for the item. Example: "Tip rack 1"
        position (dict, optional): Spatial position. Example: {"x": 10, "y": 10, "z": 0}
        content (list, optional): List of item contents. Defaults to [].

    Returns:
        dict: Base item data.
    """
    if content is None:
        content = []
    data = {
        "platform": platform,
        "name": name,
        "position": {
            "x": position["x"],
            "y": position["y"],
            "z": position["z"],
        },
        "locked": locked,
        "content": content.copy(),
        "snappedAnchor": snappedAnchor
    }
    if platformData is not None:
        data["platformData"] = platformData
    if containerData is not None:
        data["containerData"] = containerData

    validate_item(data)

    return deepcopy(data)
