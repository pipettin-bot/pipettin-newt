# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate
from .schemas import hl_protocols

#### Generic high-level protocol generators ####

def protocol_hl(name: str,
                workspace: str,
                steps: list = None,
                description: str = "A high-level pipettin' protocol.",
                template: str = "",
                templateDefinition: dict = None,
                templateFields: dict = None,
                **kwargs):
    """High-level step protocol generator.
    Example:
    >>> protocol_hl(**hl_protocols.EXAMPLE_HL_PROTOCOL)
    """

    if steps is None:
        steps = []
    if templateDefinition is None:
        templateDefinition = dict()
    if templateFields is None:
        templateFields = dict()

    protocol = {
        "name": name,
        "description": description,
        "workspace": workspace,
        "steps": steps,
        "template": template,
        "templateDefinition": templateDefinition,
        "templateFields": templateFields
    }

    protocol.update(**kwargs)

    validate(protocol, schema=hl_protocols.hl_protocol_schema)

    return deepcopy(protocol)


#### Generic high-level step generators ####

# TODO: implement other step generatos.

def make_tip(mode="reuse", item="any", discardItem="any", **kwargs):
    tip_data = {
        "mode": mode,
        "item": item,
        "discardItem": discardItem
    }
    tip_data.update(**kwargs)
    return deepcopy(tip_data)

def step_transfer(
    volume: float,
    source_value: str,
    source_by: str,
    source_platform: str,
    target_value: str,
    target_by: str,
    target_platform: str,
    tool: str,
    treat_source_as: str = "same",  # or 'for_each'
    treat_volume_as: str = "fixed_each",  # or 'fixed_total', 'for_each_target_tag'
    volume_by_tag: str = None,  # Needed for 'for_each_target_tag'
    tip: dict = None,
    order: int = 0,
    name: str = None):
    """Transfer step generator.

    Example:
    >>> from jsonschema import validate
    >>> import newt
    >>> validate(instance=newt.schemas.hl_protocols.EXAMPLE_HL_PROTOCOL,
    >>>          schema=newt.schemas.hl_protocols.hl_protocol_schema)

    Args:
        volume (float): Volume to transfer.
        source_value (str): Value used to select source contents (see "source_by").
        target_value (str): Value used to select target contents (see "target_by").
        source_by (str): Property used to select source contents (e.g. "name", "tags", "index", etc.).
        target_by (str): Property used to select target contents (e.g. "name", "tags", "index", etc.).
        source_platform (str): Name of the platform item where the source content is.
        target_platform (str): Name of the platform item where the target content is.
        tool (str): Name of the liquid handling tool to use for the transfer (e.g. a micropipette).
        order (int, optional): Order index for the step. Defaults to 0.
        name (str, optional): Name for the step. Defaults to None.
        tip (dict, optional): Tip selector and behavior object. Defaults to (re)using tips from any rack.

    Returns:
        dict: Definition of a transfer step.
    """

    if tip is None:
        tip = make_tip()
    if name is None:
        name = f"Step {order}"

    step_transfer_definition = {
        "source": {
            "item": source_platform,
            "by": source_by,
            "value": source_value,
            # "same", 'for_each'
            "treatAs": treat_source_as
        },
        "target": {
            "item": target_platform,
            "by": target_by,
            "value": target_value
        },
        "volume": {
            # 'fixed_each', 'fixed_total', 'for_each_target_tag'
            "type": treat_volume_as,
            "value": volume
        },
        "tip": tip,
        "tool": tool
    }
    # Add the tag if specified, needed for 'for_each_target_tag' volume type.
    if volume_by_tag is not None:
        step_transfer_definition["volume"]["tag"] = volume_by_tag

    hl_step = {"order": order,
               "name": name,
               "type": "TRANSFER",
               "definition": step_transfer_definition}

    validate(hl_step, schema=hl_protocols._hl_step_schema_dict["TRANSFER"])

    return deepcopy(hl_step)
