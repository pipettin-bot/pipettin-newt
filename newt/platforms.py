# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from jsonschema import validate
from .schemas import platforms as platforms_schema

def container_link(container: str, containerOffsetZ: float):
    return {
        "container": container,
        "containerOffsetZ": containerOffsetZ
    }


def slot_position(slotX, slotY):
    return {"slotX": slotX, "slotY": slotY}

def base_slot(
    slotName: str,
    slotPosition: dict,
    slotSize: float,
    slotHeight: float,
    slotActiveHeight: float = 0,
    containers: list = None):
    """_summary_

    Args:
        slotName (str): _description_
        slotPosition (dict): Slot XY position. Example: {"slotX": 10, "slotY": 9}
        slotActiveHeight (float): _description_
        slotSize (float): _description_
        slotHeight (float): _description_
        containers (list, optional): List of container links. Defaults to empty list if None.
    """
    # Defaults
    if containers is None:
        containers = []
    # Construct
    slot = {
        "slotName": slotName,
        "slotPosition": slot_position(**slotPosition),
        "slotActiveHeight": slotActiveHeight,
        "slotSize": slotSize,
        "slotHeight": slotHeight,
        "containers": containers,
    }
    # Validate
    validate(slot, platforms_schema.calibrated_slot_schema)
    return slot

def base_platform(
    platform_type: str,
    name: str,
    activeHeight: float,
    height: float,
    width: float = 0.0,
    length: float = 0.0,
    diameter: float = 0.0,
    containers: list = None,
    slots: list = None,
    description="A platform with properties shared with all platforms",
    color= "#cf947a",
    rotation=0):

    if containers is None:
        containers = []
    if slots is None:
        slots = []

    data =  {
        "type": platform_type,
        "name": name,
        "width": width,
        "length": length,
        "height": height,
        "activeHeight": activeHeight,
        "diameter": diameter,
        "description": description,
        "color": color,
        "rotation": rotation,
        "containers": containers,
        "slots": slots
    }

    validate(data, platforms_schema.base_platform_schema)

    return data

def platform_discard(
    name: str,
    platform_type="BUCKET",
    description: str = "Generic trash container.",
    color: str = "#cf947a",
    **kwargs):
    # Barebones platform
    data = base_platform(name=name,
                         description=description,
                         color=color,
                         platform_type=platform_type,
                         **kwargs)

    del data["slots"]
    del data["containers"]
    del data["diameter"]

    # Validate.
    validate(data, platforms_schema.platform_schema_bucket)

    return data

def platform_petri(
    name: str,
    diameter: float,
    height: float,
    maxVolume: float,
    description: str = "Generic petri dish.",
    platform_type="PETRI_DISH",
    color: str = "#c3c3c3",
    **kwargs):

    # Barebones platform.
    data = base_platform(name=name,
                         description=description,
                         color=color,
                         platform_type=platform_type,
                         diameter=diameter,
                         height=height,
                         **kwargs)

    # Generic pipetting parameters.
    data["maxVolume"] = maxVolume

    del data["width"]
    del data["length"]
    del data["slots"]
    del data["containers"]

    # Validate.
    validate(data, platforms_schema.platform_schema_petri)

    return data

base_grid_platform_defaults = {
    "firstWellCenterX": 10.5,
    "firstWellCenterY": 10,
    "wellDiameter": 4.5,
    "wellSeparationX": 9.0,
    "wellSeparationY": 9.0,
    "wellsColumns": 12,
    "wellsRows": 8,
}

def base_grid_platform(
    name: str,
    platform_type: str,
    # 2D well plate grid parameters.
    firstWellCenterX: float,
    firstWellCenterY: float,
    wellDiameter: float,
    wellSeparationX: float,
    wellSeparationY: float,
    wellsColumns: float,
    wellsRows: float,
    containers: list = None,
    description: str = "Platform with contents in a square grid.",
    color: str = "#8a1eF6",
    **kwargs):

    if containers is None:
        containers = []

    # Barebones platform.
    data = base_platform(name=name,
                         description=description,
                         platform_type=platform_type,
                         color=color,
                         containers=containers,
                         **kwargs)

    # 2D well plate grid parameters.
    data.update({
        "firstWellCenterX": firstWellCenterX,
        "firstWellCenterY": firstWellCenterY,
        "wellDiameter": wellDiameter,
        "wellSeparationX": wellSeparationX,
        "wellSeparationY": wellSeparationY,
        "wellsColumns": wellsColumns,
        "wellsRows": wellsRows
    })

    del data["slots"]
    del data["diameter"]

    # Validate.
    validate(data, platforms_schema.platform_schema_grid)

    return data


def platform_tip_rack(description: str = "Tip rack platform.",
                      platform_type="TIP_RACK",
                      **kwargs):

    # Barebones "grid" platform
    data = base_grid_platform(description=description,
                              platform_type=platform_type,
                              **kwargs)

    # Validate.
    validate(data, platforms_schema.platform_schema_tip_rack)

    return data


def platform_tube_rack(description: str = "Tube rack platform (square grid).",
                       platform_type="TUBE_RACK",
                       **kwargs):

    # Barebones "grid" platform
    data = base_grid_platform(description=description,
                              platform_type=platform_type,
                              **kwargs)

    # Validate.
    validate(data, platforms_schema.platform_schema_tube_rack)

    return data


def platform_custom(description: str = "Tube rack platform (square grid).",
                    platform_type="CUSTOM",
                    activeHeight=0,
                    **kwargs):

    # Barebones "grid" platform
    data = base_platform(description=description,
                         platform_type=platform_type,
                         activeHeight=activeHeight,
                         **kwargs)

    del data["containers"]

    # Validate.
    validate(data, platforms_schema.platform_schema_custom)

    return data

def platform_anchor(width: float, length: float,
                    description: str = "Curb-like structure to align platform items to the workspace.",
                    anchorOffsetX: float = -10,
                    anchorOffsetY: float = -10,
                    anchorOffsetZ: float = 0,
                    color: str = "#000000",
                    platform_type="ANCHOR",
                    **kwargs):

    # Barebones "grid" platform
    data = base_platform(description=description,
                         length=length, width=width,
                         platform_type=platform_type,
                         color=color,
                         **kwargs)

    del data["containers"]
    del data["slots"]
    del data["diameter"]

    data.update({
        "anchorOffsetX": anchorOffsetX,
        "anchorOffsetY": anchorOffsetY,
        "anchorOffsetZ": anchorOffsetZ
    })

    # Validate.
    validate(data, platforms_schema.platform_schema_anchor)

    return data
