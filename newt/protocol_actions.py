# Copyright (C) 2023-2024 Nicolás A. Méndez
#
# This file is part of "newt".
#
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.


# BASIC ACTIONS ####

def base_action(cmd, args=None) -> dict:
    action = {
        "cmd": cmd
    }
    if args is not None:
        action["args"] = args
    return action

def action_home(axis=None) -> dict:
    if axis is None:
        action = base_action(cmd="HOME")
    else:
        action = base_action(cmd="HOME", args = {"which": axis})

    return action

def action_comment(text="") -> dict:
    action = base_action(
        cmd="COMMENT",
        args = {
          "text": text
        }
    )
    return action

def action_gcode(commands: list, wait:bool=None, check:bool=None, timeout:int=None) -> dict:
    action = base_action(cmd="GCODE")
    action["GCODE"] = commands.copy()
    action["exec_opts"] = action_opts(wait, check, timeout)
    return action

def action_opts(wait:bool=None, check:bool=None, timeout:int=None) -> dict:
    opts = {}
    if wait is not None:
        opts["wait"] = wait
    if check is not None:
        opts["check"] = check
    if timeout is not None:
        opts["timeout"] = timeout
    return opts

# UTILS ####

def filter_selector(value, by="name") -> dict:
    return {
        "by": by,
        "value": value
    }

def content_locator(item:str, value:str, select_by:str) -> dict:
    locator = {
        "item": item,
        "selector": filter_selector(value, select_by)
    }
    return locator

# PLATFORM-BASED ACTIONS ####

def action_pick_tip(item:str=None, tool:str=None, value=None, select_by="name") -> dict:
    action = base_action(
        cmd="PICK_TIP",
        args = {"tool": tool} | content_locator(item, value, select_by)
    )
    return action

def action_discard_tip(item:str=None, tool:str=None, value=None, select_by="name") -> dict:
    action = action_pick_tip(
        item=item, tool=tool, value=value, select_by=select_by
    )
    action["cmd"] = "DISCARD_TIP"
    return action

def action_load_liquid(volume=None, item=None, tool:str=None, value=None, select_by="name") -> dict:
    action = base_action(
        cmd="LOAD_LIQUID",
        args = {"volume": volume, "tool": tool} | content_locator(item, value, select_by)
    )
    return action

def action_drop_liquid(volume=None, item=None, tool:str=None, value=None, select_by="name") -> dict:
    action = action_load_liquid(
        volume=volume, item=item, tool=tool, value=value, select_by=select_by
    )
    action["cmd"] = "DROP_LIQUID"
    return action

# COORDINATE-BASED ACTIONS ####

def action_load_liquid_coords(volume=None, coords=None, tool=None, used_volume=None) -> dict:
    action = base_action(
        cmd="LOAD_LIQUID",
        args = {
          "coords": coords,
          "tube": {"volume": used_volume},
          "volume": volume,
          "tool": tool
        }
    )
    return action

def action_pick_tip_coords(coords:dict=None, tool:str=None, volume:float=0.0,
                           tip_max_volume:float=0.0, tip_length:float=0.0) -> dict:
    action = base_action(
        cmd="PICK_TIP",
        args = {
          "coords": coords,
          "tool": tool,
          "tip": {'maxVolume': tip_max_volume,
                  'tipLength': tip_length,
                  'volume': volume}
        }
    )
    return action

def action_drop_liquid_coords(volume=None, coords=None, tool=None, used_volume=None) -> dict:
    action = action_load_liquid_coords(
        volume=volume, coords=coords, tool=tool, used_volume=used_volume
    )
    action["cmd"] = "DROP_LIQUID"
    return action
