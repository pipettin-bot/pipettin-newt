# Copyright (C) 2023-2024 Nicolás A. Méndez
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

from copy import deepcopy
from .schemas.contents import validate_content

#### Generic content generators ####

def base_content(container, index, name, position, volume=0, tags: list = None, **kwargs) -> dict:
    """Base definition of a content, with platform-dependent position.

    Args:
        type (str, optional): Content type definition. Example: ""
        index (int, optional): Index for the content in its box. Example: 0
        name (str, optional): Name for the tip, such as "tip1" or "tube1". Example: ""
        position (dict, optional): Column and row of the tip. Example: {"col": 0, "row": 0}, {"x": 0, "y": 0, "z": 0}
        tags (list, optional): Tags associated to this content. Example: []

    NOTE: maxVolume, tipLength, volume may be required in kwargs for some functionality. They were required by 'base_content_coord'.

    Returns:
        dict: Content definition.
    """
    
    if tags is None:
        tags = []

    data = {
        "container": container,
        "index": index,
        "name": name,
        "position": position,
        "tags": tags,
        "volume": volume
    }
    
    data.update(**kwargs)

    validate_content(data)
    
    return deepcopy(data)
