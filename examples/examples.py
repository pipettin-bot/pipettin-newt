# Copyright (C) 2023-2024 Nicolás A. Méndez & Pedro Rozadas
# 
# This file is part of "newt".
# 
# "newt" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "newt" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "newt". If not, see <https://www.gnu.org/licenses/>.

import os, sys
module_path = os.path.expanduser("~/Projects/GOSH/gosh-col-dev/pipettin-grbl/newt/")
sys.path.append(module_path)

import newt

newt.contents.base_content()

newt.workspaces.EXAMPLE_WORKSPACE

newt.workspaces.base_workspace()

newt.protocols.base_protocol()

newt.protocol_actions.action_pick_tip()
