# Object generation module for the Pipettin project

Convenience functions to generate and validate JSON representations of pipetting objects (i.e. protocols, steps, labware, etc.). Also includes JSON schemas for them.

Status: pre-release, under development.

For examples see [mix/usage.ipynb](mix/usage.ipynb) (might be outdated).
